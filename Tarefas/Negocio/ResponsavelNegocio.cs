﻿using Tarefas.Interfaces;
using Tarefas.Model;


namespace Tarefas.Negocio
{
    public class ResponsavelNegocio : IResponsavelNegocio
    {
        private readonly IResponsavelRepositorio _responsavelRepositorio;
        public ResponsavelNegocio(IResponsavelRepositorio responsavelRepositorio)
        {
            _responsavelRepositorio = responsavelRepositorio;
        }

        public void CadastrarResponsavel(Responsavel responsavel)
        {

            if (responsavel == null)
            {
                throw new Exception("O Objeto responsável está vazio!");
            }

            var responsavelConsulta = _responsavelRepositorio.BuscarResponsavelPorId(responsavel.Id);
            if (responsavelConsulta != null)
            {
                throw new Exception("Responsavel já existe!");
            }

            if (responsavel.Nome == null || responsavel.Nome.Length < 2)
            {
                throw new Exception("Nome inválido!");
            }

            _responsavelRepositorio.CadastrarResponsavel(responsavel);
        }

        public Responsavel BuscarResponsavelPorId(string id)
        {
           var responsavel = _responsavelRepositorio.BuscarResponsavelPorId(id);

            if(responsavel == null )
            {
                return null;
            }
            if(responsavel.Ativo == false)
            {
                throw new Exception("Responsável Inativo!");
            }
            return responsavel;
        }

        public IList<Responsavel> BuscarResponsaveis()
        {
            var responsaveis = _responsavelRepositorio.BuscarTodosResponsaveis();
            return responsaveis;
        }


    }
}
