﻿using Tarefas.Interfaces;
using Tarefas.Model;

namespace Tarefas.Tarefas
{
    public class TarefaNegocio : ITarefaNegocio
    {
        private readonly ITarefaRepositorio _tarefaRepositorio;
        private readonly IResponsavelRepositorio _responsavelRepositorio;

        public TarefaNegocio(ITarefaRepositorio tarefaRepositorio,
                             IResponsavelRepositorio responsavelRepositorio)
        {
            _tarefaRepositorio = tarefaRepositorio;
            _responsavelRepositorio = responsavelRepositorio;
          
            
         }

        public void CadastrarTarefa(Tarefa tarefa)
        {
            
            var tarefaEncontrada = _tarefaRepositorio.VisualizarTarefas(tarefa.Id);

            
            if (tarefaEncontrada == null)
            {
                _tarefaRepositorio.CadastrarTarefa(tarefa);

            }
            else
            {
                throw new Exception("Tarefa já existe na lista!");
            }
            
        }

        public void TrocarResponsavelTarefa(Responsavel responsavel, int idTarefa)
        {   
            var responsavelTarefa = _responsavelRepositorio.BuscarResponsavelPorId(responsavel.Id);

            if (responsavelTarefa != null)
            {
                _tarefaRepositorio.TrocarResponsavelTarefa(responsavel, idTarefa);
            }
            else
            {
                throw new Exception("Responsável não existe!");
            }
        }

        public Tarefa VisualizarTarefas(int id)
        {
            var tarefaEncontrada = _tarefaRepositorio.VisualizarTarefas(id);
            if(tarefaEncontrada == null)
            {
                throw new Exception("Tarefa não existe!");
                
            }

            return tarefaEncontrada;
        }

        public IList<Tarefa> ListarTarefas()
        {
            return _tarefaRepositorio.ListarTarefas();
        }

        public IList<Tarefa> ListarTarefasPorResponsavel(string idResponsavel)
        {
            var todasTarefas = _tarefaRepositorio.ListarTarefas();

            var tarefasDoResponsavel = new List<Tarefa>();

            foreach(var tar in todasTarefas)
            {
                if(tar.Responsavel != null)
                {
                    if(tar.Responsavel.Id == idResponsavel)                    
                        tarefasDoResponsavel.Add(tar);
                }
            }

            return tarefasDoResponsavel;
        }
    }
}



