﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarefas.Model
{
    public class Tarefa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public Responsavel? Responsavel { get; set; }

        public StatusTarefa Status { get; set; }
    }

    public enum StatusTarefa
    {
        Vazio,
        Em_Andamento,
        Concluída,
        Cancelada
    }
}
