﻿namespace Tarefas.Model
{
    public class Responsavel
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
    }
}
