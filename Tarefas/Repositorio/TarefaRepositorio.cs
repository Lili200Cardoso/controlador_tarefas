﻿using Tarefas.Interfaces;
using Tarefas.Model;

namespace Tarefas.Repositorio
{
    public class TarefaRepositorio : ITarefaRepositorio
    {
        //Criando uma lista vazia, para receber as tarefas...
        IList<Tarefa> listaTarefas;

        public TarefaRepositorio()
        {
            //Estanciando a lista vazia
            listaTarefas = new List<Tarefa>();
        }
        public void CadastrarTarefa(Tarefa tarefa)
        {
            listaTarefas.Add(tarefa);
        }


        public Tarefa VisualizarTarefas(int id)
        {

            return listaTarefas.FirstOrDefault(tarefa => tarefa.Id == id);
           
        }
        public IList<Tarefa> ListarTarefas()
        {

            return listaTarefas;

        }

        public void TrocarResponsavelTarefa(Responsavel responsavel,int idTarefa)
        {

            foreach (var item in listaTarefas)
            {
                if(item.Id == idTarefa)
                {
                    item.Responsavel = responsavel;
                    break;
                }
            }
        }

     
    }
}

 

