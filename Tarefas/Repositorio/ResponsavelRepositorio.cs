﻿using Tarefas.Interfaces;
using Tarefas.Model;
using System.Linq;

namespace Tarefas.Repositorio
{
    public class ResponsavelRepositorio : IResponsavelRepositorio
    {
        //Criando uma lista vazia, para receber os responsaveis...
        IList<Responsavel> listaResponsaveis;

        public  ResponsavelRepositorio()
        {
            //Estanciando a lista vazia
            listaResponsaveis = new List<Responsavel>();

        }    

        public Responsavel BuscarResponsavelPorId(string id = "") 
        {
            return listaResponsaveis.FirstOrDefault(responsavel => responsavel.Id == id);
        }

        public IList<Responsavel> BuscarTodosResponsaveis()
        {
            return listaResponsaveis;           
        }

        public void CadastrarResponsavel(Responsavel responsavel)
        {   
            //Populando a lista de responsaveis...
            listaResponsaveis.Add(responsavel);
        }
    }
}
