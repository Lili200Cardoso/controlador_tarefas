﻿using Tarefas.Model;

namespace Tarefas.Interfaces
{
    public interface ITarefaRepositorio
    {
        IList<Tarefa> ListarTarefas();
        void CadastrarTarefa(Tarefa tarefa);
        Tarefa VisualizarTarefas(int id);
        void TrocarResponsavelTarefa(Responsavel responsavel, int idTarefa);
    }
}
