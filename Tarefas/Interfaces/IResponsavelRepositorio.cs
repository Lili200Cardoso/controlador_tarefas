﻿using Tarefas.Model;


namespace Tarefas.Interfaces
{
    public interface IResponsavelRepositorio
    {
        Responsavel BuscarResponsavelPorId(string id);
        IList<Responsavel> BuscarTodosResponsaveis();
        void CadastrarResponsavel(Responsavel responsavel);
       
    }
}
