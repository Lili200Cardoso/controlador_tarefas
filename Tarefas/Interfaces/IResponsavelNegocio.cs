﻿using Tarefas.Model;

namespace Tarefas.Interfaces
{
    public interface IResponsavelNegocio
    {
        Responsavel BuscarResponsavelPorId(string id);
        void CadastrarResponsavel(Responsavel responsavel);
        IList<Responsavel> BuscarResponsaveis();
    }
}
