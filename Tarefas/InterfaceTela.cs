﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tarefas.Model;

namespace Tarefas
{
    internal class InterfaceTela
    {
        public static string ImprimirTraco(int quantidade, string tipo)
        {
            string linha = string.Empty;

            for (int i = 0; i < quantidade; i++)
            {
                linha = linha + tipo;
            }

            return linha;
        }

        public static void ImprimirMenuInicial()
        {
            var linha = InterfaceTela.ImprimirTraco(100, "*");
            Console.WriteLine($"{linha}");
            Console.WriteLine("                             BEM-VINDOS AO CONTROLADOR DE TAREFAS                    ");
            Console.WriteLine($"{linha}");
            Console.WriteLine("\n\n");
            Console.WriteLine("ESCOLHA A OPÇÃO DESEJADA:");
            Console.WriteLine("\n\n");
            Console.WriteLine("1 - CADASTRAR RESPONSÁVEL");
            Console.WriteLine("2 - CADASTRAR TAREFA");
            Console.WriteLine("3 - MINHAS TAREFAS");
            Console.WriteLine("4 - OUTRAS TAREFAS");
            Console.WriteLine("5 - ALTERAR RESPONSÁVEL TAREFA");
            Console.WriteLine("6 - LISTAR RESPONSÁVEL");
            Console.WriteLine("7 - SAIR");
            Console.WriteLine("\n\n");
            Console.WriteLine($"{linha}");
        }

        internal static Responsavel CadastroResponsal()
        {
            Console.Clear();
            var responsavel = new Responsavel();
            Console.Write("Nome: ");
            var nome = Console.ReadLine();
            responsavel.Nome = nome;
            

            //Gerando Id
            responsavel.Id = Guid.NewGuid().ToString();
            responsavel.Ativo = true;

            return responsavel;
        }
        internal static Tarefa CadastroTarefa()
        {
            Console.Clear();
            var tarefa = new Tarefa();


            //Gerando Id
            Random rand = new Random();
            tarefa.Id = rand.Next(1, 9999999);

            //Nome
            Console.Write("Nome: ");
            var nome = Console.ReadLine();
            tarefa.Nome = nome;

            //Descrição 
            Console.Write("Descrição: ");
            var descricao = Console.ReadLine();
            tarefa.Descricao = descricao;

            //Status
            tarefa.Status = StatusTarefa.Vazio;

            return tarefa;
        }

        internal static string[] AlterarResponsavel()
        {
            Console.Clear();
            string[] chaves = new string[2];
            Console.WriteLine("Informe o Id do responsável: ");            
            var idResponsavel = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("Informe o id da Tarefa: ");
            var idTarefa = Console.ReadLine();

            chaves[0] = idResponsavel;
            chaves[1] = idTarefa;

            return chaves;            
        }
        public static void MensagemProcessamentoFinal()
        {
            Console.WriteLine("Pressione uma tecla para continuar...");
            Console.ReadKey(); 
        }
        
    }
}
