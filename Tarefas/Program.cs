﻿using Microsoft.Extensions.DependencyInjection;
using Tarefas;
using Tarefas.Interfaces;
using Tarefas.Model;
using Tarefas.Negocio;
using Tarefas.Repositorio;
using Tarefas.Tarefas;


//var linha = InterfaceTela.ImprimirTraco(100, "*");





var serviceCollection = new ServiceCollection();
serviceCollection.AddSingleton<IResponsavelRepositorio, ResponsavelRepositorio>();
serviceCollection.AddSingleton<IResponsavelNegocio, ResponsavelNegocio>();
serviceCollection.AddSingleton<ITarefaRepositorio, TarefaRepositorio>();
serviceCollection.AddSingleton<ITarefaNegocio, TarefaNegocio>();

var serviceProvider = serviceCollection.BuildServiceProvider();

var _responsavelNegocio = serviceProvider.GetService<IResponsavelNegocio>();
////var novoResponsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true };
////responsavelNegocio.CadastrarResponsavel(novoResponsavel);

var _tarefaNegocio = serviceProvider.GetService<ITarefaNegocio>();
////var novaTarefa = new Tarefa
////{
////    Id = 1,
////    Nome = "Teste",
////    Descricao = "Testando",
////    Status = StatusTarefa.Em_Andamento,
////    Responsavel = novoResponsavel,
////};
////tarefaNegocio.CadastrarTarefa(novaTarefa);



//Console.WriteLine("Programa Encerrado!");






var opcaoEscolhida = "";

while (opcaoEscolhida != "7")
{
    Console.Clear();
    InterfaceTela.ImprimirMenuInicial();
    Console.Write("IMFORME A OPÇÃO DESEJADA: ");
    opcaoEscolhida = Console.ReadLine();

    switch (opcaoEscolhida)
    {

        case "1":
            var responsavel = InterfaceTela.CadastroResponsal();
            _responsavelNegocio.CadastrarResponsavel(responsavel);
            Console.WriteLine($"Responsável {responsavel.Nome}, cadastrado com sucesso");
            InterfaceTela.MensagemProcessamentoFinal();
            break;
        case "2":
            var tarefa = InterfaceTela.CadastroTarefa();
            _tarefaNegocio.CadastrarTarefa(tarefa);
            Console.WriteLine($"Tarefa de nome {tarefa.Nome}, cadastrada com sucesso");
            InterfaceTela.MensagemProcessamentoFinal();
            break;
        case "3":
            Console.Clear();
            var usuario = _responsavelNegocio.BuscarResponsaveis().FirstOrDefault();
            var minhasTarefas = _tarefaNegocio.ListarTarefasPorResponsavel(usuario.Id);
            foreach (var tar in minhasTarefas)
            {
                Console.WriteLine($"Id: {tar.Id} | Nome: {tar.Nome} | Descrição: {tar.Descricao} | Status: {tar.Status.ToString()} | Responsável: {tar.Responsavel.Nome}");
            }
            InterfaceTela.MensagemProcessamentoFinal();
            break;
        case "4":
            Console.Clear();
            var todasTarefas = _tarefaNegocio.ListarTarefas();
            foreach (var tar in todasTarefas)
            {
                Console.WriteLine($"Id: {tar.Id} | " +
                    $"Nome: {tar.Nome} | " +
                    $"Descrição: {tar.Descricao} | " +
                    $"Status: {tar.Status.ToString()} | " +
                    $"Responsável: {(tar.Responsavel != null ? tar.Responsavel.Nome : '*')}");//Operador ternário
            }
            InterfaceTela.MensagemProcessamentoFinal();
            break;        
        case "5":
            var chaves = InterfaceTela.AlterarResponsavel();
            var novoResponsavel = _responsavelNegocio.BuscarResponsavelPorId(chaves[0]);
            _tarefaNegocio.TrocarResponsavelTarefa(novoResponsavel, int.Parse(chaves[1]));
            InterfaceTela.MensagemProcessamentoFinal();
            break;
        case "6":
            Console.Clear();
            var responsaveis = _responsavelNegocio.BuscarResponsaveis();

            foreach (var resp in responsaveis)
            {
                Console.WriteLine($"Id: {resp.Id} - Nome: {resp.Nome} - Ativo: {resp.Ativo.ToString()}");
            }

            InterfaceTela.MensagemProcessamentoFinal();
            break;
        case "7":
            break;
        default:
            Console.WriteLine("Erro. Escolha uma opção válida.");
            InterfaceTela.MensagemProcessamentoFinal();
            break;
    }
}







//public int Id { get; set; }
//public string Nome { get; set; }
//public string Descricao { get; set; }
//public Responsavel? Responsavel { get; set; }

//public StatusTarefa Status { get; set; }



//    Console.WriteLine("ESCOLHA A OPÇÃO DESEJADA:");
//    Console.WriteLine("\n\n");
//    Console.WriteLine("1 - CADASTRAR RESPONSÁVEL");
//    Console.WriteLine("2 - CADASTRAR TAREFA");
//    Console.WriteLine("3 - MINHAS TAREFAS");
//    Console.WriteLine("4 - OUTRAS TAREFAS");
//    Console.WriteLine("5 - ALTERAR RESPONSÁVEL TAREFA");
//    Console.WriteLine("6 - SAIR");
//    Console.WriteLine("\n\n");
//    Console.WriteLine($"{linha}");
//}





//
//var nome = new Responsavel();
//Console.WriteLine("Digite o nome do responsável pela tarefa: ");
//nome.Nome = Console.ReadLine();



//Console.WriteLine(nome);