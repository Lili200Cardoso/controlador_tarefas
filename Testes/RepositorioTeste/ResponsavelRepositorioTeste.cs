﻿using Microsoft.Extensions.DependencyInjection;
using Tarefas.Interfaces;
using Tarefas.Model;
using Tarefas.Repositorio;

namespace Testes.RepositorioTeste
{
    [TestClass]
    public class ResponsavelRepositorioTeste
    {
        
        private readonly IResponsavelRepositorio _responsavelRepositorio;

        public ResponsavelRepositorioTeste()
        {
            var serviceCollection = new ServiceCollection();

            //A Interface é injetada na classe...
            serviceCollection.AddSingleton<IResponsavelRepositorio, ResponsavelRepositorio>();

            //Compila as injeções acima e "armazena para serem consumidas..."
            var serviceProvider = serviceCollection.BuildServiceProvider();

            //Disponibiliza através da consulat da interface entre o GetService e a classe, que simula a instância do objeto....
            _responsavelRepositorio = serviceProvider.GetService<IResponsavelRepositorio>();

        }

        [TestMethod]
        public void CadastrarResponsavelComSucesso()
        {
            var novoResponsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true };

            _responsavelRepositorio.CadastrarResponsavel(novoResponsavel);

            Assert.IsNotNull(novoResponsavel);


        }


        [TestMethod]
        public void BuscarResponsavelPorIdComSucesso()
        {
            var idResponsavel = "ABC";
            var novoResponsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true };

            _responsavelRepositorio.BuscarResponsavelPorId(novoResponsavel.Id);

            Assert.AreEqual(novoResponsavel.Id, idResponsavel);
  

        }
    }
}


