using Microsoft.Extensions.DependencyInjection;
using Tarefas.Interfaces;
using Tarefas.Model;
using Tarefas.Repositorio;

namespace Testes.RepositorioTeste
{

    [TestClass]
    public class TarefaRepositorioTeste
    {
        private readonly ITarefaRepositorio _tarefaRepositorio;
        private readonly IResponsavelRepositorio _responsavelRepositorio;
        public TarefaRepositorioTeste()
        {
            var serviceCollection = new ServiceCollection();

            //A Interface � injetada na classe...
            serviceCollection.AddSingleton<ITarefaRepositorio, TarefaRepositorio>();
            serviceCollection.AddSingleton<IResponsavelRepositorio, ResponsavelRepositorio>();

            //Compila as inje��es acima e "armazena para serem consumidas..."
            var serviceProvider = serviceCollection.BuildServiceProvider();

            //Disponibiliza atrav�s da consulat da interface entre o GetService e a classe, que simula a inst�ncia do objeto....
            _tarefaRepositorio = serviceProvider.GetService<ITarefaRepositorio>();
            _responsavelRepositorio = serviceProvider.GetService<IResponsavelRepositorio>();

        }


        [TestMethod]
        public void CadastrarTarefaComSucesso()
        {
            var novaTarefa = new Tarefa { Id = 1, Descricao = "Testando", Nome = "Teste", Status = StatusTarefa.Em_Andamento };

            _tarefaRepositorio.CadastrarTarefa(novaTarefa);

            Assert.IsNotNull(novaTarefa);

        }


        [TestMethod]
        public void VisualizarTarefaComSucesso()
        {
            var tarefaId = 1;
            var novaTarefa = new Tarefa { Id = 1, Descricao = "Testando", Nome = "Teste", Status = StatusTarefa.Em_Andamento };

            _tarefaRepositorio.CadastrarTarefa(novaTarefa);
            var tarefa = _tarefaRepositorio.VisualizarTarefas(tarefaId);

            Assert.AreEqual(tarefa.Id, tarefaId);

        }

        [TestMethod]
        public void TrocarResponsavelTarefaComSucesso()
        {
            var tarefaId = 2;
            var novoResponsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true };
            var novaTarefa = new Tarefa { Id = 2, Descricao = "Testando", Nome = "Teste", Status = StatusTarefa.Em_Andamento };

            _tarefaRepositorio.CadastrarTarefa(novaTarefa);
            _responsavelRepositorio.CadastrarResponsavel(novoResponsavel);
            _tarefaRepositorio.TrocarResponsavelTarefa(novoResponsavel, 2);
            var tarefa = _tarefaRepositorio.VisualizarTarefas(tarefaId);

            Assert.AreEqual(tarefa.Id, tarefaId);

        }

    }

}










