﻿using Microsoft.Extensions.DependencyInjection;
using Moq;
using Tarefas.Tarefas;
using Tarefas.Interfaces;
using Tarefas.Negocio;
using Tarefas.Model;
using Tarefas.Repositorio;

namespace Testes.RepositorioTeste
{

    [TestClass]
    public class TarefaNegocioTeste
    {
        private  readonly ITarefaNegocio _tarefaNegocio;
        private readonly IResponsavelNegocio _responsavelNegocio;
        
        public TarefaNegocioTeste()
        {
            var serviceCollection = new ServiceCollection();

            //A Interface é injetada na classe...
            serviceCollection.AddSingleton<ITarefaNegocio, TarefaNegocio>();
            serviceCollection.AddSingleton<IResponsavelNegocio, ResponsavelNegocio>();
            serviceCollection.AddSingleton<ITarefaRepositorio,TarefaRepositorio>();
            serviceCollection.AddSingleton<IResponsavelRepositorio,ResponsavelRepositorio>();

            //Compila as injeções acima e "armazena para serem consumidas..."
            var serviceProvider = serviceCollection.BuildServiceProvider();

            //Disponibiliza através da consulat da interface entre o GetService e a classe, que simula a instância do objeto....
            _tarefaNegocio = serviceProvider.GetService<ITarefaNegocio>();
            _responsavelNegocio = serviceProvider.GetService<IResponsavelNegocio>();
            

        }


        [TestMethod]
        public void CadastrarTarefaExistente()
        {
            var tarefa = new Tarefa { Id = 1, Descricao = "Testando", Nome = "Teste", Status = StatusTarefa.Em_Andamento };
            var novaTarefa = new Tarefa { Id=1, Descricao = "Testando", Nome = "Teste", Status = StatusTarefa.Em_Andamento };
            
            _tarefaNegocio.CadastrarTarefa(tarefa);

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.CadastrarTarefa(novaTarefa));
            Assert.AreEqual("Tarefa já existe na lista!", ex.Message);
        }

        [TestMethod]
        public void TrocarResponsavelInexistente()
        {            
            var idTarefa = 1;
            var responsavelTarefa = new Responsavel { Id = "ABC" };

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.TrocarResponsavelTarefa(responsavelTarefa, idTarefa));
            Assert.AreEqual("Responsável não existe!", ex.Message);

        }

        [TestMethod]
        public void VisualizarTarefaInexistente()
        {
            var tarefa = 0;

            var ex = Assert.ThrowsException<Exception>(() => _tarefaNegocio.VisualizarTarefas(tarefa));
            Assert.AreEqual("Tarefa não existe!", ex.Message);

        }

        [TestMethod]
        public void ListarTarefasComSucesso()
        {
            var tarefaId = 1;
            var novaTarefa = new Tarefa { Id = 1, Descricao = "Testando", Nome = "Teste", Status = StatusTarefa.Em_Andamento };

            _tarefaNegocio.CadastrarTarefa(novaTarefa);
            var tarefa = _tarefaNegocio.ListarTarefas();

            Assert.IsNotNull( tarefaId);

        }

        [TestMethod]
        public void ListarTarefasPorResponsavelComSucesso()
        {            
            var novoResponsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true };
            var novaTarefa = new Tarefa { Id = 1, Descricao = "Testando", Nome = "Teste", Status = StatusTarefa.Em_Andamento, Responsavel = novoResponsavel };

            _responsavelNegocio.CadastrarResponsavel(novoResponsavel);
            _tarefaNegocio.CadastrarTarefa(novaTarefa);            
            var tarefa = _tarefaNegocio.ListarTarefasPorResponsavel(novoResponsavel.Id);

            Assert.IsNotNull(tarefa.ToArray()[0].Id);


        }
    }
}

