﻿using Microsoft.Extensions.DependencyInjection;
using Moq;
using Tarefas.Tarefas;
using Tarefas.Interfaces;
using Tarefas.Negocio;
using Tarefas.Model;
using Tarefas.Repositorio;

namespace Testes.RepositorioTeste
{

    [TestClass]
    public class ResponsavelNegocioTeste
    {
   
        private readonly IResponsavelNegocio _responsavelNegocio;
        public ResponsavelNegocioTeste()
        {
            var serviceCollection = new ServiceCollection();

            //A Interface é injetada na classe...
            serviceCollection.AddSingleton<ITarefaNegocio, TarefaNegocio>();
            serviceCollection.AddSingleton<IResponsavelNegocio, ResponsavelNegocio>();
            serviceCollection.AddSingleton<ITarefaRepositorio, TarefaRepositorio>();
            serviceCollection.AddSingleton<IResponsavelRepositorio, ResponsavelRepositorio>();

            //Compila as injeções acima e "armazena para serem consumidas..."
            var serviceProvider = serviceCollection.BuildServiceProvider();

            //Disponibiliza através da consulat da interface entre o GetService e a classe, que simula a instância do objeto....
           
            _responsavelNegocio = serviceProvider.GetService<IResponsavelNegocio>();

        }

        [TestMethod]
        public void CadastrarResponsavelComSucesso()
        {
            var novoResponsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true};

            _responsavelNegocio.CadastrarResponsavel(novoResponsavel);

            Assert.IsNotNull(novoResponsavel);

        }

        [TestMethod]
        public void CadastrarResponsavelVazio()
        {
            Responsavel novoResponsavel = null;

            var ex = Assert.ThrowsException<Exception>(() => _responsavelNegocio.CadastrarResponsavel(novoResponsavel));
            Assert.AreEqual("O Objeto responsável está vazio!", ex.Message);
        }

        [TestMethod]
        public void CadastrarResponsavelExistente()
        {
            var responsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true };
            _responsavelNegocio.CadastrarResponsavel(responsavel);

            var novoResponsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true };

            var ex = Assert.ThrowsException<Exception>(() => _responsavelNegocio.CadastrarResponsavel(novoResponsavel));
            Assert.AreEqual("Responsavel já existe!", ex.Message);

        }

        [TestMethod]
        public void CadastrarResponsavelSemNome()
        {
            var responsavel = new Responsavel { Id = "ABC", Nome = "", Ativo = true };
            
            var ex = Assert.ThrowsException<Exception>(() => _responsavelNegocio.CadastrarResponsavel(responsavel));
            Assert.AreEqual("Nome inválido!", ex.Message);

        }

        [TestMethod]
        public void CadastrarResponsavelComNomeInvalido()
        {
            var responsavel = new Responsavel { Id = "ABC", Nome = "l", Ativo = true };

            var ex = Assert.ThrowsException<Exception>(() => _responsavelNegocio.CadastrarResponsavel(responsavel));
            Assert.AreEqual("Nome inválido!", ex.Message);

        }


        [TestMethod]
        public void BuscarResponsavelPorIdInativo()
        {
            var responsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = false };
            _responsavelNegocio.CadastrarResponsavel(responsavel);

            var ex = Assert.ThrowsException<Exception>(() => _responsavelNegocio.BuscarResponsavelPorId("ABC"));
            Assert.AreEqual("Responsável Inativo!", ex.Message);

        }

        [TestMethod]
        public void BuscarResponsaveisComSucesso()
        {
            var idResponsavel = "ABC";
            var novoResponsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true };

            _responsavelNegocio.BuscarResponsaveis();

            Assert.AreEqual(novoResponsavel.Id, idResponsavel);


        }

    }
}



//var idResponsavel = "ABC";
//var novoResponsavel = new Responsavel { Id = "ABC", Nome = "Lili", Ativo = true };

//_responsavelRepositorio.BuscarResponsavelPorId(novoResponsavel.Id);

//Assert.AreEqual(novoResponsavel.Id, idResponsavel);



//public IList<Responsavel> BuscarResponsaveis()
//{
//    var responsaveis = _responsavelRepositorio.BuscarTodosResponsaveis();
//    return responsaveis;
//}


